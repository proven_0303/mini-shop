
import React from 'react';
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';



function Detail(){
   
    let state = useSelector((state)=>{ return state.shoes })
    let dispatch = useDispatch();
    
    return(
    <div className="content-wp">
        
        <div className="row">
  
            <div className="col-md-6">
              <img src={'https://codingapple1.github.io/shop/shoes'+(state.shoesModel[0].id + 1 )+'.jpg'} width="80%" />
            </div>
      
            <div className="col-md-6 mt-4">
                <h4>{`제품명 : ${state.shoesModel[0].title}`}</h4>
                <p>{`원산지 : ${state.shoesModel[0].content}`}</p>
                <p> 가격: {state.shoesModel[0].price}</p>
              <button className="btn btn-danger" onClick={()=>{
              }}
              >장바구니</button> 
            </div>
          </div>

    </div>
    )
}

export default Detail;