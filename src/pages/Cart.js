import React from "react";
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {clearModels, calculateTotals,increase,decrease,removeModel} from './../store/store.js';


function Cart(){
    const {shoesModel,totalValue,totalQuantity} = useSelector((state)=>state.shoes);
    const dispatch = useDispatch()
    useEffect(()=>{
        dispatch(calculateTotals())
    },[shoesModel])

    return(
    <div className="cart-list">
        <h2>
            Total Value: <span>{totalValue.toFixed(0)}</span>
        </h2>
        <p>Total: {totalQuantity}</p>
        <p>초기화 : <button onClick={()=>{dispatch(clearModels())}}>reset</button></p>
        {shoesModel.map((shoes)=>{
            return <ShoesModel key={shoes.id}{...shoes} />
        })}
        
    </div>
    )
}



function ShoesModel({id, title, price ,img, quantity}){
    const dispatch = useDispatch()
    return(
        <div className="grid grid-cols-3 gap-4 justify-items-center shoesItem"> 
            <div>
                <h3 className="pr-3">{`car model:${title}`}</h3>
                <button className="pl-12"  onClick={()=>dispatch(removeModel({id}))}>
                    삭제
                </button>
            </div>
            <img className='object-cover h-48 w-96' src={img} alt='N.A.' />
            <div className="m-3">
                <button className="pl-12" onClick={()=>dispatch(increase({id}))}>
                    +
                </button>
                <p className='p-3'>가격: {price}</p>
                <p className='p-3'>수량: {quantity}</p>
                <button onClick={() => dispatch(decrease({ id }))}>
                    -
                </button>
            </div>

        </div>
    )
}

export default Cart;