import React, { useState } from 'react';


const Accordion = ({ title, children }) => {//children :컴포넌트 태크 사이의 내용을 보여주는 props
    const [isOpen, setOpen] = useState(false);
    return (
        <div className='wrapper'>
            <div className="accordion-wrapper2 test">
                <div className={`accordion-title2 ${isOpen ? "open" : ""}`} onClick={() => setOpen(!isOpen)}>
                    {title}
                </div>
                <div className={`accordion-item2 ${!isOpen ? "collapsed" : ""}`}>
                    <div className="accordion-content2">{children}</div>
                </div>
            </div>
            {/*console.log({children})*/} 
        </div>
    );
};

export default Accordion;