import { configureStore, createSlice } from '@reduxjs/toolkit'
import shoesData from '../shoesData';


let shoes = createSlice({
  name : 'shoes',
  initialState : {shoesModel : shoesData, number: 0, totalValue: 0},//데이터를 불러옴
  reducers:{
    clearModels: (state) => {
        state.shoesModel = [];
    },
    removeModel: (state, { payload }) => {
        state.shoesModel = state.shoesModel.filter(
            (model) => model.id !== payload.id
        );
    },
    increase:(state,{payload})=>{
        const shoesModel = state.shoesModel.find((model)=>model.id===payload.id)
        shoesModel.quantity = shoesModel.quantity + 1
    },
    decrease:(state,{payload})=>{
        const shoesModel = state.shoesModel.find((model)=>model.id===payload.id)
        if(shoesModel.quantity > 0){
          shoesModel.quantity = shoesModel.quantity - 1;
        }else{
          shoesModel.quantity = 0
        }
    },
    calculateTotals : (state)=>{
        let totalQuantity = 0;
        let totalValue = 0;
        state.shoesModel.forEach((model) => {
            totalQuantity += model.quantity;
            totalValue += model.quantity * model.price;
        });
        state.totalQuantity = totalQuantity;
        state.totalValue = totalValue;
    }
}
})
export let {clearModels, calculateTotals,increase,decrease,removeModel} = shoes.actions

let user = createSlice({
  name : 'user',
  initialState : {name : 'kim', age: 20},
  reducers : {
    changeName(state){//state : 기존 state를 뜻함
      //return {name : 'park', age: '20'}
      state.name = 'park' // array/object의 경우 직접수정해도 stat변경됨 : immer.js 의 도움으로
    }
  }

})

export let {changeName} = user.actions 

let isActive = createSlice({
  name : 'isActive',
  initialState : { isState : 'true', isNumber : 0, name : 'test'},
  reducers :{
    changeState(state){
      //state.isState = false
      state.isNumber = 20
    }
  }
})

export let {changeState} = isActive.actions 



export default configureStore({
  reducer: { 
    user : user.reducer,
    isActive : isActive.reducer,
    shoes: shoes.reducer
  }
}) 