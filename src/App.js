import logo from './logo.svg';
import menu from './img/ico-menu.png';
import './App.css';
import {Button,Navbar,Container,Nav} from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Routes, Route, Link, useNavigate, Outlet } from 'react-router-dom';
import Detail from './pages/Detail.js';
import Cart from './pages/Cart.js';
import Accordion from './component/Accordion.js';
import { useDispatch, useSelector } from 'react-redux';
import {changeState, changeName} from './store/store.js';

import { Navigation, Pagination, A11y } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';



function App() {

  let state = useSelector((state)=>{ return state });
  //console.log(state)
  let dispatch = useDispatch() // store.js로 요청보내주는 함수

  let [active, activeChange] = useState(false);
  let [fade, setFade] = useState('');
  let [sticky, setSticky] = useState(false);

  //let [shoes, setShoes] = useState(shosData);
  //console.log(shoes)

  useEffect(()=>{
    function handleScroll(){
      const currentScrollPosition = window.pageYOffset;
      if(currentScrollPosition >= 56){
        setSticky(true)
      }else{
        setSticky(false)
      }
    }
    window.addEventListener('scroll', handleScroll);
    // Clean up function
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };

  },[])

  return (
    <div className="App">
      <header className={sticky ? 'h-fixed' : ''}>
        <Navbar bg="dark" variant="dark">
          <Container>
          <div className={'menu-icon-wp ' + fade}><Nav.Link><img src={menu} className="menu-icon" onClick = {()=>{
              activeChange(!active);
              setFade(!fade)
            }} /></Nav.Link></div>
            <Navbar.Brand href="/">
              <img src={logo} className="logo d-inline-block align-top" alt="logo" />
              logo
            </Navbar.Brand>
          </Container>
        </Navbar>
        { 
          active == true ? <NavList />  : null
        }
      </header>

      <Routes>
        <Route path="/" element={<div className="main-layout">메인페이지
          <div className='main-slide'>
            <Swiper
              // install Swiper modules
              modules={[Navigation, Pagination, A11y]}
              spaceBetween={10}
              slidesPerView={1}
              navigation
              pagination={{ clickable: true }}
            >
              <SwiperSlide>
                <img src={require('./img/shoes1.jpg')} className='visual-img' alt='비주얼이미지1' />
              </SwiperSlide>
              <SwiperSlide><img src={require('./img/shoes2.jpg')} className='visual-img' alt='비주얼이미지2' /></SwiperSlide>
              <SwiperSlide><img src={require('./img/shoes3.jpg')} className='visual-img' alt='비주얼이미지3' /></SwiperSlide>
              <SwiperSlide><img src={require('./img/shoes1.jpg')} className='visual-img' alt='비주얼이미지4' /></SwiperSlide>
            </Swiper>
          </div>

          <div className='container'>
              <div className='row'>
                  {state.shoes.shoesModel.map((shoes)=>{
                    return <Card key={shoes.id}{...shoes} />
                  })} 
              </div>
           </div> 

          <div>{state.isActive.isNumber}</div>
          <button onClick={()=>{
              dispatch(changeState())
            }}>버튼</button>

        </div>} />
        <Route path="/detail" element={<Detail></Detail>}  />
        <Route path="/cart" element={<Cart></Cart>}   />
        <Route path="/about" element={<About/>}>
          <Route path="greeting" element={
          <div>
              <h5>Q &amp; A</h5>
              <div className="wrapper1">
                <Accordion title='질문: 회사는 언제 창립되었나요?'>
                  11Sunlight reaches Earth's atmosphere and is scattered in all directions by
                  all the gases and particles in the air. Blue light is scattered more than
                  the other colors because it travels as shorter, smaller waves. This is why
                  we see a blue sky most of the time.
                </Accordion>
                <Accordion title="질문: 회사는 어디에 위치하고 있나요?">
                  22It's really hot inside Jupiter! No one knows exactly how hot, but
                  scientists think it could be about 43,000°F (24,000°C) near Jupiter's
                  center, or core.
                </Accordion>
                <Accordion title="질문 : 회사의 비전과 목표는 무엇인가요?">
                  33A black hole is an area of such immense gravity that nothing -- not even
                  light -- can escape from it.
                </Accordion>
              </div>

          </div>} />
          <Route path="location" element={<div>위치 정보</div>} />
        </Route>
      </Routes>

    </div>
  );
}


function Card({id, title, content ,price, quantity}){
  let state = useSelector((state)=>{ return state.shoes });
  let navigate = useNavigate();

  return(
    <div className='col-md-4'>
      
      <img src={'https://codingapple1.github.io/shop/shoes'+(id+1)+'.jpg'} width="80%" onClick={()=>{navigate('/detail/'+(id+1))}} />
        <h4>{`제품명 : ${title}`}</h4>
        <p>{`원산지 : ${content}`}</p>
        <p> 가격: {price}</p>
        <p> 수량 : {quantity}</p>
    </div>
  )
}


function About(){
  return(
    <div>
      <h4 className="title">회사정보</h4>
      <Outlet></Outlet>
    </div>
  )
}


function NavList(){
  let navigate = useNavigate();
  return(
    <div className='nav-wrap'>
        <Nav defaultActiveKey="/" className="flex-column">
          <ul>
            <li><Nav.Link  eventKey="link0"  onClick={()=>{navigate('/');}} >Home</Nav.Link></li>
            <li><Nav.Link  eventKey="link1"  onClick={()=>{navigate('/detail');}} >Detail</Nav.Link></li>
            <li><Nav.Link  eventKey="link2"  onClick={()=>{navigate('/cart')}} >Cart</Nav.Link></li>
            <li><Nav.Link  eventKey="link3"  onClick={()=>{navigate('/about/greeting')}} >About</Nav.Link></li>
          </ul>
        </Nav>
      </div>
  )
}
export default App;
